#include	"LBMD2Q9.h"

#include	<iostream>
#include	<fstream>
#include	<string>

#include	"format.h"

const double LBMD2Q9::weight[LBMD2Q9::DIRECTIONS] = {4. / 9., 1. / 9., 1. / 9., 1. / 9., 1. / 9.,
1. / 36., 1. / 36., 1. / 36., 1. / 36.};

LBMD2Q9::LBMD2Q9(const unsigned int sizeS, const unsigned int sizeT, const double omega, int obstacles[]):
	sizeS_(sizeS + 2),
	sizeT_(sizeT + 2),
	omega_(omega),
	ld_(sizeS_ + PADDING),
	fieldSize_(ld_ * sizeT_){
	
	rho_ = new double[fieldSize_];
	ux_  = new double[fieldSize_];
	uy_  = new double[fieldSize_];
	
	for ( unsigned int i = 0; i < DIRECTIONS; ++i ){
		src_.push_back( new double[fieldSize_] );
		dst_.push_back( new double[fieldSize_] );
	}

	if (obstacles == NULL) {

	}
	
/*
	if (obstacles != NULL) {
		for (int i = 0; i < obstacles.length(); ++i) {
			addNoSlipObstacle (obstacles[i]);
		}
	}

	for ( unsigned int s = 0; s < sizeS_; ++s ){
		addNoSlipObstacle( s, 0 );
		addNoSlipObstacle( s, sizeT_ - 1 );
	}
	for ( unsigned int t = 0; t < sizeT_; ++t ){
		addNoSlipObstacle( 0, t );
		addNoSlipObstacle( sizeS_ - 1, t );
	}
	*/

	init(1.0, 0.0, 0.0);
}

LBMD2Q9::~LBMD2Q9(){
	for ( auto& ptr : src_ ){
		delete[] ptr;
	}
	for ( auto& ptr : dst_ ){
		delete[] ptr;
	}

	delete[] rho_;
	delete[] ux_;
	delete[] uy_;
}

void LBMD2Q9::addNoSlipObstacle( const unsigned int s, const unsigned int t ){
	noSlipObstacles_.push_back( getLinIndex( s, t ) );
}

void LBMD2Q9::run( const unsigned int steps ){
	for ( unsigned int i = 0; i < steps; ++i ){
		streamAndCollide();
		//std::cout << totrho_ << std::endl;
	}
	simulationStep_ += steps;
}

void LBMD2Q9::init(const double rho, const double ux, const double uy){
	for ( unsigned int t = 0; t < sizeT_; ++t ){
		for ( unsigned int s = 0; s < sizeS_; ++s ){
			const unsigned int currentIndex = getLinIndex( s + 0, t + 0 );

			//initialize moving field
			ux_[currentIndex] = ux;
			uy_[currentIndex] = uy;
			rho_[currentIndex] = rho;

			const double constantTerm = rho_[currentIndex] - 1.5 * (ux_[currentIndex] * ux_[currentIndex] + uy_[currentIndex] * uy_[currentIndex]);
			double feq = weight[C] * (constantTerm);

			src_[C][currentIndex] = feq;

			feq =  weight[N] * (constantTerm + 3 * uy_[currentIndex] + 4.5 * uy_[currentIndex] * uy_[currentIndex]);
			src_[N][currentIndex] = feq;
			feq = weight[S] * (constantTerm - 3 * uy_[currentIndex] + 4.5 * uy_[currentIndex] * uy_[currentIndex]);
			src_[S][currentIndex] = feq;

			feq = weight[W] * (constantTerm - 3 * ux_[currentIndex] + 4.5 * ux_[currentIndex] * ux_[currentIndex]);
			src_[W][currentIndex] = feq;
			feq = weight[E] * (constantTerm + 3 * ux_[currentIndex] + 4.5 * ux_[currentIndex] * ux_[currentIndex]);
			src_[E][currentIndex] = feq;

			feq = weight[NW] * (constantTerm + 3 * (-ux_[currentIndex] + uy_[currentIndex]) + 4.5 * (-ux_[currentIndex] + uy_[currentIndex]) * (-ux_[currentIndex] + uy_[currentIndex]));
			src_[NW][currentIndex] = feq;
			feq = weight[NE] * (constantTerm + 3 * (ux_[currentIndex] + uy_[currentIndex]) + 4.5 * (ux_[currentIndex] + uy_[currentIndex]) * (ux_[currentIndex] + uy_[currentIndex]));
			src_[NE][currentIndex] = feq;

			feq = weight[SW] * (constantTerm + 3 * (-ux_[currentIndex] - uy_[currentIndex]) + 4.5 * (-ux_[currentIndex] - uy_[currentIndex]) * (-ux_[currentIndex] - uy_[currentIndex]));
			src_[SW][currentIndex] = feq;
			feq = weight[SE] * (constantTerm + 3 * (+ux_[currentIndex] - uy_[currentIndex]) + 4.5 * (+ux_[currentIndex] - uy_[currentIndex]) * (+ux_[currentIndex] - uy_[currentIndex]));
			src_[SE][currentIndex] = feq;
		
			totrho_ = 0;

			//calculated quantities

			rho_[currentIndex] =
				src_[C][currentIndex] +
				src_[N][currentIndex] +
				src_[S][currentIndex] +
				src_[E][currentIndex] +
				src_[W][currentIndex] +
				src_[NW][currentIndex] +
				src_[NE][currentIndex] +
				src_[SW][currentIndex] +
				src_[SE][currentIndex];

			totrho_ += rho_[currentIndex];

			ux_[currentIndex] =
				src_[E][currentIndex] +
				src_[SE][currentIndex] +
				src_[NE][currentIndex] -
				src_[W][currentIndex] -
				src_[NW][currentIndex] -
				src_[SW][currentIndex];
			ux_[currentIndex] /= rho_[currentIndex];

			uy_[currentIndex] =
				src_[N][currentIndex] -
				src_[S][currentIndex] +
				src_[NW][currentIndex] +
				src_[NE][currentIndex] -
				src_[SW][currentIndex] -
				src_[SE][currentIndex];
			uy_[currentIndex] /= rho_[currentIndex];

			//std::cout << rho_[currentIndex] << "\t" << ux_[currentIndex] << "\t" << uy_[currentIndex] << std::endl;
			//std::cout << src_[N][currentIndex] << "\t" << src_[S][currentIndex] << "\t" << src_[E][currentIndex] << "\t" << src_[W][currentIndex] << std::endl;
			//std::cout << src_[NE][currentIndex] << "\t" << src_[NW][currentIndex] << "\t" << src_[SE][currentIndex] << "\t" << src_[SW][currentIndex] << std::endl;
		}
	}
}
void LBMD2Q9::boundaryHandling(){
	//velocity of boundary
	const double wx = 0.08;
	//const double wy = 0;
	
	//simulation box boundaries
	unsigned int currentIndex = getLinIndex( 0, 0 );
	src_[NE][currentIndex] = src_[SW][currentIndex + ld_ + 1];
	currentIndex = getLinIndex( 0, sizeT_ - 1 );
	src_[SE][currentIndex] = src_[NW][currentIndex - ld_ + 1] + 6.0 * weight[NW] * wx;
	for ( unsigned int s = 1; s < sizeS_ - 1; ++s ){
		currentIndex = getLinIndex( s, 0 );
		src_[N][currentIndex] = src_[S][currentIndex + ld_];
		src_[NW][currentIndex] = src_[SE][currentIndex + ld_ - 1];
		src_[NE][currentIndex] = src_[SW][currentIndex + ld_ + 1];

		currentIndex = getLinIndex( s, sizeT_ - 1 );
		src_[S][currentIndex]  = src_[N][currentIndex - ld_];
		src_[SW][currentIndex] = src_[NE][currentIndex - ld_ - 1] - 6.0 * weight[NE] * wx;
		src_[SE][currentIndex] = src_[NW][currentIndex - ld_ + 1] + 6.0 * weight[NW] * wx;
	}
	currentIndex = getLinIndex( sizeS_ - 1, 0 );
	src_[NW][currentIndex] = src_[SE][currentIndex + ld_ - 1];
	currentIndex = getLinIndex( sizeS_ - 1, sizeT_ - 1 );
	src_[SW][currentIndex] = src_[NE][currentIndex - ld_ - 1] - 6.0 * weight[NE] * wx;

	for ( unsigned int t = 1; t < sizeT_ - 1; ++t ){
		currentIndex = getLinIndex( 0, t );
		src_[E][currentIndex] = src_[W][currentIndex + 1];
		src_[NE][currentIndex] = src_[SW][currentIndex + ld_ + 1];
		src_[SE][currentIndex] = src_[NW][currentIndex - ld_ + 1];

		currentIndex = getLinIndex( sizeS_ - 1, t );
		src_[W][currentIndex] = src_[E][currentIndex - 1];
		src_[NW][currentIndex] = src_[SE][currentIndex + ld_ - 1];
		src_[SW][currentIndex] = src_[NE][currentIndex - ld_ - 1];
	}
	
	//custom boundaries
	for ( auto& currentIndex : noSlipObstacles_ ){
		src_[N][currentIndex] = src_[S][currentIndex + ld_];
		src_[S][currentIndex] = src_[N][currentIndex - ld_];
		src_[E][currentIndex] = src_[W][currentIndex + 1];
		src_[W][currentIndex] = src_[E][currentIndex - 1];

		src_[NW][currentIndex] = src_[SE][currentIndex + ld_ - 1];
		src_[NE][currentIndex] = src_[SW][currentIndex + ld_ + 1];
		src_[SW][currentIndex] = src_[NE][currentIndex - ld_ - 1];
		src_[SE][currentIndex] = src_[NW][currentIndex - ld_ + 1];
	}
}
void LBMD2Q9::streamAndCollide(){
	totrho_ = 0;

	boundaryHandling();

	for ( unsigned int t = 1; t < sizeT_ - 1; ++t ){
		for ( unsigned int s = 1; s < sizeS_ - 1; ++s ){
			const unsigned int currentIndex = getLinIndex( s + 0, t + 0 );

			//stream
			dst_[C][currentIndex] = src_[C][currentIndex];

			dst_[N][currentIndex] = src_[N][getLinIndex( s + 0, t - 1 )];
			dst_[S][currentIndex] = src_[S][getLinIndex( s + 0, t + 1 )];
			dst_[E][currentIndex] = src_[E][getLinIndex( s - 1, t + 0 )];
			dst_[W][currentIndex] = src_[W][getLinIndex( s + 1, t + 0 )];

			dst_[NW][currentIndex] = src_[NW][getLinIndex( s + 1, t - 1 )];
			dst_[NE][currentIndex] = src_[NE][getLinIndex( s - 1, t - 1 )];
			dst_[SW][currentIndex] = src_[SW][getLinIndex( s + 1, t + 1 )];
			dst_[SE][currentIndex] = src_[SE][getLinIndex( s - 1, t + 1 )];

			//calculated quantities

			rho_[currentIndex] =
				dst_[C][currentIndex] +
				dst_[N][currentIndex] +
				dst_[S][currentIndex] +
				dst_[E][currentIndex] +
				dst_[W][currentIndex] +
				dst_[NW][currentIndex] +
				dst_[NE][currentIndex] +
				dst_[SW][currentIndex] +
				dst_[SE][currentIndex];

			totrho_ += rho_[currentIndex];

			ux_[currentIndex] =
				dst_[E][currentIndex] +
				dst_[SE][currentIndex] +
				dst_[NE][currentIndex] -
				dst_[W][currentIndex]  -
				dst_[NW][currentIndex] -
				dst_[SW][currentIndex];
			ux_[currentIndex] /= rho_[currentIndex];

			uy_[currentIndex] =
				dst_[N][currentIndex] -
				dst_[S][currentIndex] +
				dst_[NW][currentIndex] +
				dst_[NE][currentIndex] -
				dst_[SW][currentIndex] -
				dst_[SE][currentIndex];
			uy_[currentIndex] /= rho_[currentIndex];
			
			//collide
			const double constantTerm = rho_[currentIndex] - 1.5 * (ux_[currentIndex] * ux_[currentIndex] + uy_[currentIndex] * uy_[currentIndex]);
			double feq = weight[C] * (constantTerm);
			dst_[C][currentIndex] -= omega_ * (dst_[C][currentIndex] - feq);

			feq = weight[N] * (constantTerm + 3 * uy_[currentIndex] + 4.5 * uy_[currentIndex] * uy_[currentIndex]);
			dst_[N][currentIndex] -= omega_ * (dst_[N][currentIndex] - feq);
			feq = weight[S] * (constantTerm - 3 * uy_[currentIndex] + 4.5 * uy_[currentIndex] * uy_[currentIndex]);
			dst_[S][currentIndex] -= omega_ * (dst_[S][currentIndex] - feq);

			feq = weight[W] * (constantTerm - 3 * ux_[currentIndex] + 4.5 * ux_[currentIndex] * ux_[currentIndex]);
			dst_[W][currentIndex] -= omega_ * (dst_[W][currentIndex] - feq);
			feq = weight[E] * (constantTerm + 3 * ux_[currentIndex] + 4.5 * ux_[currentIndex] * ux_[currentIndex]);
			dst_[E][currentIndex] -= omega_ * (dst_[E][currentIndex] - feq);

			feq = weight[NW] * (constantTerm + 3 * (-ux_[currentIndex] + uy_[currentIndex]) + 4.5 * (-ux_[currentIndex] + uy_[currentIndex]) * (-ux_[currentIndex] + uy_[currentIndex]));
			dst_[NW][currentIndex] -= omega_ * (dst_[NW][currentIndex] - feq);
			feq = weight[NE] * (constantTerm + 3 * (ux_[currentIndex] + uy_[currentIndex]) + 4.5 * (ux_[currentIndex] + uy_[currentIndex]) * (ux_[currentIndex] + uy_[currentIndex]));
			dst_[NE][currentIndex] -= omega_ * (dst_[NE][currentIndex] - feq);

			feq = weight[SW] * (constantTerm + 3 * (-ux_[currentIndex] - uy_[currentIndex]) + 4.5 * (-ux_[currentIndex] - uy_[currentIndex]) * (-ux_[currentIndex] - uy_[currentIndex]));
			dst_[SW][currentIndex] -= omega_ * (dst_[SW][currentIndex] - feq);
			feq = weight[SE] * (constantTerm + 3 * (+ux_[currentIndex] - uy_[currentIndex]) + 4.5 * (+ux_[currentIndex] - uy_[currentIndex]) * (+ux_[currentIndex] - uy_[currentIndex]));
			dst_[SE][currentIndex] -= omega_ * (dst_[SE][currentIndex] - feq);

			rho_[currentIndex] =
				dst_[C][currentIndex] +
				dst_[N][currentIndex] +
				dst_[S][currentIndex] +
				dst_[E][currentIndex] +
				dst_[W][currentIndex] +
				dst_[NW][currentIndex] +
				dst_[NE][currentIndex] +
				dst_[SW][currentIndex] +
				dst_[SE][currentIndex];

			totrho_ += rho_[currentIndex];

			ux_[currentIndex] =
				dst_[E][currentIndex] +
				dst_[SE][currentIndex] +
				dst_[NE][currentIndex] -
				dst_[W][currentIndex] -
				dst_[NW][currentIndex] -
				dst_[SW][currentIndex];
			ux_[currentIndex] /= rho_[currentIndex];

			uy_[currentIndex] =
				dst_[N][currentIndex] -
				dst_[S][currentIndex] +
				dst_[NW][currentIndex] +
				dst_[NE][currentIndex] -
				dst_[SW][currentIndex] -
				dst_[SE][currentIndex];
			uy_[currentIndex] /= rho_[currentIndex];
		}
	}

	dst_.swap( src_ );
}

void LBMD2Q9::writeVTKToFile(std::string vtkPath) const{
	vtkPath += "_";
	std::ostringstream converter;
	converter << timestep_;
	vtkPath += converter.str();
	vtkPath += ".vtk";

	std::cout << "writing to: " << vtkPath << std::endl;

	std::ofstream fout(vtkPath);

	fout << "# vtk DataFile Version 4.0" << std::endl;
	fout << "SiwiRVisFile" << std::endl;
	fout << "ASCII" << std::endl;
	fout << "DATASET STRUCTURED_POINTS" << std::endl;
	fout << fmt::format( "DIMENSIONS {0} {1} {2}", sizeS_ - 2, sizeT_ - 2, 1 ) << std::endl;
	fout << "ORIGIN 0 0 0" << std::endl;
	fout << "SPACING 1 1 1" << std::endl;
	fout << fmt::format( "POINT_DATA {0}", ((sizeS_ - 2) * (sizeT_ - 2)) ) << std::endl;

	fout << std::endl;
		
	fout << "SCALARS flags double 1" << std::endl;
	fout << "LOOKUP_TABLE default" << std::endl;
	for ( unsigned int t = 1; t < sizeT_ - 1; ++t ){
		for ( unsigned int s = 1; s < sizeS_ - 1; ++s ){
			fout << "1" << std::endl;
		}
	}

	fout << std::endl;

	fout << "SCALARS density double 1" << std::endl;
	fout << "LOOKUP_TABLE default" << std::endl;
	for ( unsigned int t = 1; t < sizeT_ - 1; ++t ){
		for ( unsigned int s = 1; s < sizeS_ - 1; ++s ){
			fout << getrho( s, t ) << std::endl;
		}
	}

	fout << std::endl;

	fout << "VECTORS velocity double" << std::endl;
	for ( unsigned int t = 1; t < sizeT_ - 1; ++t ){
		for ( unsigned int s = 1; s < sizeS_ - 1; ++s ){
			fout << fmt::format( "{0} {1} {2}", getux( s, t ), getuy( s, t ), 0 ) << std::endl;
		}
	}

	fout.close();
}

void LBMD2Q9::writePythonToFile() const{
	std::ofstream fout( fmt::format( "python{0}.dat", simulationStep_ ) );

	for ( unsigned int t = 1; t < sizeT_ - 1; ++t ){
		for ( unsigned int s = 1; s < sizeS_ - 1; ++s ){
			fout << fmt::format( "{0} {1} {2} {3} {4}", s, t, getux( s, t ), getuy( s, t ), getrho( s, t )) << std::endl;
		}
	}

	fout.close();
}
